﻿using SW_ProcessHandle;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SW_Salesforce_CTI_MakeACall
{
    class Program
    {
        //  static StringBuilder sb = new StringBuilder();
        static readonly string PRIMARY_PROCESS_NAME = "SW_Salesforce_CTI_Integration";
        static string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static string logPath = String.Format(currentPath + "\\SW_Salesforce_CTI_MakeACall.txt");
        static string phoneNumber;
        static string activityId;
        static StringBuilder sb = new StringBuilder();

        [STAThread]
        static void Main(string[] args)
        {
            sb.AppendLine("****************************************************");
            sb.AppendLine("********Start SW_Salesforce_CTI_MakeACall********");

            ProcessHandle processHandle = new ProcessHandle(PRIMARY_PROCESS_NAME); 
            if (!processHandle.IsSalesforceCompositeIntegrationRunning())
            {
                MessageBoxEx.Show(String.Format(@"שלום {0}
יש להפעיל את סרגל ההתקשרות לפני ביצוע שיחה
שנמצא בשולחן העבודה", Environment.UserName));
               // Process.Start(Config.CtiIntegrationPath);
                Process.GetCurrentProcess().Dispose();
                Application.Exit();
                return;
            }
           
            if (Debugger.IsAttached)
            {
                args = new string[] { "aaaa:", "0524236351" };
            } 
            try
            {
                if (args != null && args.Length > 0)
                {
                    sb.AppendLine("********SW_Salesforce_CTI_MakeACall Args = " + args[1] + ";" + args[2]);
                    phoneNumber = args[1];
                    if (args.Length > 2 && !String.IsNullOrEmpty(args[2]))
                    {
                        activityId = args[2];
                    }
                    
                    AxSmartBarClient.AxClient axCtiClient = new AxSmartBarClient.AxClient();
                    axCtiClient.CreateControl();

                    //axCtiClient.MakeDirty();
                    axCtiClient.Init();

                    //To Do: send to make a call salesforce activity Id
                    axCtiClient.Agent_PressButton_MakeCall(activityId, phoneNumber, null);
                    
                }
                else
                {
                    //  sb.AppendLine("NO Args");
                    sb.AppendLine("********SW_Salesforce_CTI_MakeACall Error - NO Phone Args");
                }
            }
            catch (Exception ex)
            {
                sb.AppendLine("********SW_Salesforce_CTI_MakeACall Error - " + ex.Message);
                //throw ex;
            }
            finally
            {
                // sb.AppendLine("********End Dailing From Salesforce********");
                //  sb.AppendLine();
                //  File.AppendAllText(logPath, sb.ToString());
                sb.AppendLine("**********End SW_Salesforce_CTI_MakeACall***********");
                sb.AppendLine("****************************************************");
                File.AppendAllText(logPath, sb.ToString());
                Process.GetCurrentProcess().Dispose();
                Application.Exit();
            }

        }

       
    }
           
    static class MessageBoxEx
    {
        public static void Show(string content)
        {
            MessageBox.Show(content, "", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
        }
    }
  
}
