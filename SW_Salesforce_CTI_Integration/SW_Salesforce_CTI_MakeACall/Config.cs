﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SW_Salesforce_CTI_MakeACall
{
    public class Config
    {
        public static string CtiIntegrationPath
        {
            get
            {
                return ConfigurationManager.AppSettings["SalesforceCtiIntegrationExePath"];
            }
        }
    }
}
