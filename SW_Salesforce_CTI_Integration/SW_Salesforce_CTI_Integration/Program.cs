﻿using SW_ProcessHandle;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SW_Salesforce_CTI_Integration
{
   public class Program
    {
        static readonly string PRIMARY_PROCESS_NAME = "SW_Salesforce_CTI_Integration";

        [STAThread]
        static void Main(string[] args)
        {

            ProcessHandle processHandle = new ProcessHandle(PRIMARY_PROCESS_NAME);
            processHandle.KillOldOpenedProcess();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            WaitForCall waitingForCall = new WaitForCall();

            Application.Run(waitingForCall);
        }
    }
}
