﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace SW_Salesforce_CTI_Integration
{
    public class WaitForCall : ApplicationContext
    {   
        string currentPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        private const string CTI_GENERAL_DETAILS_ENG_PHONE_FIELD_NAME = "Caller's Telephone";
        private const string CTI_GENERAL_DETAILS_ENG_CALL_ID = "Call_ID";
        private const string CTI_GENERAL_DETAILS_ENG_INTERACTION_TYPE_FIELD_NAME = "Interaction_Type";
        private const string CTI_GENERAL_DETAILS_ENG_DNIS_FIELD_NAME = "DNIS";
        private const string CTI_GENERAL_DETAILS_ENG_WAITINGTIMEINQ_FIELD_NAME = "WaitTimeInQ";
        private const string CTI_GENERAL_DETAILS_ENG_CAMPAIGNORG_FIELD_NAME = "CampaignORG";
        private const string CTI_GENERAL_DETAILS_HEB_PHONE_FIELD_NAME = "מס' המתקשר";
        private const string CTI_GENERAL_DETAILS_HEB_AGENT_FIELD_NAME = "הסוכן";
        private const string CTI_GENERAL_DETAILS_HEB_CUSTOMER_NUMBER_FIELD_NAME = "מס' לקוח";
        private const string CTI_GENERAL_DETAILS_HEB_CAMPAIGN_FIELD_NAME = "קמפיין";

        private NotifyIcon trayIcon { get; set; }
        private string logPath { get; set; }
        private static StringBuilder sb { get; set; }
        public static AxSmartBarClient.AxClient axCtiClient;
        private string telephonyStatus = String.Empty;
        private static DateTime startTimeCall;
        private static DateTime endTimeCall;

        // public event EventHandler<ThresholdReachedEventArgs> ThresholdReached;

        public WaitForCall()
        {
            // Initialize Tray Icon
            trayIcon = new NotifyIcon()
            {
                Icon = Properties.Resources.cloud_icon,
                ContextMenu = new ContextMenu(new MenuItem[] {
                new MenuItem("Exit", Exit)//,new MenuItem("OpenChrome", TestOpenChrome)
            }),
                Visible = true
            };

            InitializeParameters();
            sb = new StringBuilder();
            logPath = String.Format(currentPath + "\\SW.Salesforce.Log.WaitingForCall.txt");

        }

        public void MyFunc(string phoneNumber)
        {
            axCtiClient.MakeDirty();
            axCtiClient.Init();
            axCtiClient.Agent_PressButton_MakeCall("1234567899999", phoneNumber, null);
        }

        private void InitializeParameters()
        {
            sb = new StringBuilder();
            axCtiClient = new AxSmartBarClient.AxClient();
            axCtiClient.CreateControl();

            int retValue = axCtiClient.Init(true, true, false, null, null, null, 0);

            axCtiClient.DialogCallDetails += AxCtiClient_DialogCallDetails;
            axCtiClient.TelephonyState += AxCtiClient_TelephonyState;
            axCtiClient.CompositeInformation += AxCtiClient_CompositeInformation;
            axCtiClient.AgentState += AxCtiClient_AgentState;
            axCtiClient.DialogAcceptCall += AxCtiClient_DialogAcceptCall;
            axCtiClient.DialogAcceptCallClose += AxCtiClient_DialogAcceptCallClose; 
            axCtiClient.DialogCallDetailsClose += AxCtiClient_DialogCallDetailsClose;
           
            //logPath = String.Format(System.Environment.GetEnvironmentVariable("TEMP") + "\\SW.Salesforce.Log.WaitForCalling.txt");
        }

        private void AxCtiClient_TelephonyState(object sender, AxSmartBarClient.__Client_TelephonyStateEvent e)
        {
           
            if (e.stateKey == "Connected")
            {
                startTimeCall = DateTime.Now;
            }
           
            //Telephony Disconnect State
            if (this.telephonyStatus == "Connected" && (e.stateKey == "Disconnected" || e.stateKey == String.Empty))
            {
                endTimeCall = DateTime.Now; 
                TimeSpan span = endTimeCall.Subtract(startTimeCall);
                OpenSalesforceEndCall(startTimeCall, endTimeCall);
            }

            this.telephonyStatus = e.stateKey;
        }
        
        private void AxCtiClient_AgentState(object sender, AxSmartBarClient.__Client_AgentStateEvent ex)
        {
            string callId = null;
            callId = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_CALL_ID);
        }
        
        private void AxCtiClient_CompositeInformation(object sender, AxSmartBarClient.__Client_CompositeInformationEvent e)
        {
            string callId = null;
            callId = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_CALL_ID);
        }

        //disconected event
        private void AxCtiClient_DialogCallDetailsClose(object sender, EventArgs e)
        {
            sb.Length = 0;
            string callId = null;
            callId = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_CALL_ID);
        }

        private void AxCtiClient_DialogCallDetails(object sender, EventArgs e)
        {
            sb.Length = 0;
            sb.AppendLine("********Start AxCtiClient_DialogCallDetails********");

            string callerNumber = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_HEB_PHONE_FIELD_NAME).Replace(" ", "%20");
            string agentName = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_HEB_AGENT_FIELD_NAME).Replace(" ", "%20");
            string interactionType = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_INTERACTION_TYPE_FIELD_NAME).Replace(" ", "%20");
            string customerNumber = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_HEB_CUSTOMER_NUMBER_FIELD_NAME).Replace(" ", "%20");
            string dnis = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_DNIS_FIELD_NAME).Replace(" ", "%20");
            string interactionId = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_CALL_ID).Replace(" ", "%20");
            string waitTimeInQ = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_WAITINGTIMEINQ_FIELD_NAME).Replace(" ", "%20");
            string campaignOrg = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_CAMPAIGNORG_FIELD_NAME).Replace(" ", "%20");
            string campaign = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_HEB_CAMPAIGN_FIELD_NAME).Replace(" ", "%20");
            
            /*
            if (String.IsNullOrEmpty(callerNumber))
            {
                callerNumber = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName(CTI_GENERAL_DETAILS_ENG_PHONE_FIELD_NAME);
            }
            */

            OpenSalesforceIncomingCall(callerNumber, agentName, interactionType, customerNumber, dnis, interactionId, waitTimeInQ, campaignOrg, campaign);

            sb.AppendLine("********End AxCtiClient_DialogCallDetails********");
        }

        private void OpenSalesforceIncomingCall(string callerNumber, string agent, string interactionType, string customerNumber, string dnis, string interactionId, string waitTimeInQ, string campaignOrg, string campaign)
        {
            string openSalesforceParams = String.Format("?callerNumber={0}&agentName={1}&interactionType={2}&customerNumber={3}&dnis={4}&interactionId={5}&waitTimeInQ={6}&campaignOrg={7}&campaign={8}", new String[] { callerNumber, agent, interactionType, customerNumber, dnis, interactionId , waitTimeInQ, campaignOrg, campaign });
            Process.Start("chrome.exe", Config.SalesforceIncomingCallUrl + openSalesforceParams);
        }

        private void AxCtiClient_DialogAcceptCallClose(object sender, EventArgs e)
        {
            sb.Length = 0;
        }

        private void AxCtiClient_DialogAcceptCall(object sender, EventArgs e)
        {
            //To Do: Open
            sb.Length = 0;
        }

        private void OpenSalesforceEndCall(DateTime startTime, DateTime endTime)
        {
            Process process = new Process();
            process.StartInfo.FileName = "chrome.exe";
            string openSalesforceParams = String.Format("?startTime={0}&endTime={1}", new String[] { startTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(" ", "%20"), endTime.ToString("yyyy-MM-dd HH:mm:ss").Replace(" ", "%20")});

            //process.StartInfo.Arguments = "--start-fullscreen --app=" + Config.SalesforceEndCallUrl;
            process.StartInfo.Arguments = Config.SalesforceEndCallUrl + openSalesforceParams;
            process.Start();
        }

        /*
private void AxCtiClient_AgentState(object sender, AxSmartBarClient.__Client_AgentStateEvent e)
{
   string ss = "";
}

private void AxCtiClient_DialogAcceptCall(object sender, EventArgs e)
{
   string ss = "";
   string yyy = ((AxSmartBarClient.AxClient)(sender)).GetCallDetailByName("");
}

private void CtiClient_AgentState(ref string StateKey, ref string StateCaption, ref bool IsReadyButtonEnabled, ref bool IsBreakButtonEnabled, ref bool IsPaperWorkButtonEnabled, ref bool IsMakeCallButtonEnabled, ref string HelpString)
{
   string ss = "";

}

private void CtiClient_DialogCallDetailsClose()
{
   string ss = "";

}

private void CtiClient_DialogCallDetails()
{
   string xxx = "";
   string yyy = "";

   //string xxx = "";
   //xxx = ctiClient.GetAcceptCallDetailByName("CallerId");
   //string yyy = ctiClient.GetAcceptCallDetailByName("ANI");
   //string zzz = ctiClient.GetAcceptCallDetailByName("Ani");
   //string bbb = ctiClient.GetAcceptCallDetailByName("ani");
}

private void CtiClient_DialogAcceptCallClose()
{
   string ss = "";
}


private void CtiClient_CompositeInformation(ref int InformationType, ref string Description, ref int Code)
{
   string ss = "";
}

private void CtiClient_DialogAcceptCall()
{


   sb.Clear();
   sb.AppendLine("********Start DialogAcceptCall********");

   sb.AppendLine("********End DialogAcceptCall**************");
   sb.AppendLine();

   File.AppendAllText(this.logPath, sb.ToString());
}
*/
        void Exit(object sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it
            //File.Delete(this.logPath);
            trayIcon.Visible = false;
            ((IDisposable)axCtiClient).Dispose();
            Application.Exit();
        }
        void MakeAACall(object sender, EventArgs e)
        {
            axCtiClient.Agent_PressButton_MakeCall(null, "0524236351", null);
        }
        
    }
}
