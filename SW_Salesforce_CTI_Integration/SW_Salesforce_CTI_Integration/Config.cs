﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace SW_Salesforce_CTI_Integration
{
    public class Config
    {
        public static string SalesforceIncomingCallUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SalesforceIncomingCallUrl"];
            }
        }
        public static string SalesforceEndCallUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["SalesforceEndCallUrl"];
            }
        }

    }
}
