﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SW_Salesforce_Sieble_Integration
{
    class SiebelScheduleUrl : ISiebelExecute
    {
        public string SiebleUrl;
        public SiebelScheduleUrl(string siebelUserName, string siebelUserPassword, string siebelRedirectPage, string opportunityId, string bundleProductId, string productCode, string serviceRegionNumber, string opportunityNumber, string isServiceRegionChanged)
        {
            this.SiebleUrl = String.Format(@"http://{0}/callcenter_heb/start.swe?SWECmd=ExecuteLogin&SWEUserName={1}&SWEPassword={2}&SWEAC=SWECmd=InvokeMethod&SWEMethod=InboundOceanCall&SWEService=T4+Ocean+Integration+Service&SFId={3}&OppNum={8}&OrderTypeCD=Sales&OrderType=TradeIn&SrvReg={4}&ReqAction=Install&NumOfLines=1&SFLineId0={5}&CnclDsptchFlg={9}&Product0={6}&NavigateTo={7}"
               , new string[] { Config.SiebelEnvDomain, siebelUserName, siebelUserPassword, opportunityId, serviceRegionNumber, bundleProductId, productCode, siebelRedirectPage, opportunityNumber, isServiceRegionChanged });
        }

        public void Execute()
        {
            Process.Start("IEXPLORE.EXE", this.SiebleUrl);
        }
    }
}
