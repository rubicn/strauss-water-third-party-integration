﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SW_Salesforce_Sieble_Integration
{
    class SiebelCompanyUrl : ISiebelExecute
    {
        public string SiebleUrl;

        public SiebelCompanyUrl(string siebelCustomerCode)
        {
            this.SiebleUrl = String.Format(@"http://{0}/callcenter_heb/start.swe?SWECmd=ExecuteLogin&SWEUserName={1}&SWEPassword={2}&SWEAC=SWECmd=InvokeMethod&SWEMethod=DoIT&SWEService=T4+Ocean+Integration+Service&AccountId={3}"
               , new string[] { Config.SiebelEnvDomain, Config.SiebelUserName, Config.SiebelUserPassword, siebelCustomerCode });
        }

        public void Execute()
        {
            Process.Start("IEXPLORE.EXE", this.SiebleUrl);
        }
    }
}
