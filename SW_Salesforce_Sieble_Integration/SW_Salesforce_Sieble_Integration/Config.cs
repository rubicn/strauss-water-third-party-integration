﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace SW_Salesforce_Sieble_Integration
{
    public class Config
    {
        public static string SiebelUserName
        {
            get
            {
                return ConfigurationManager.AppSettings["SiebelUserName"];
            }
        }

        public static string SiebelUserPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["SiebelUserPassword"];
            }
        }

        public static string SiebelEnvDomain
        {
            get
            {
                return ConfigurationManager.AppSettings["SiebelEnvDomain"];
            }
        }
        public static string SiebelExplorerTitleToClose
        {
            get
            {
                return ConfigurationManager.AppSettings["SiebelExplorerTitleToClose"];
            }
        }
    }
}
