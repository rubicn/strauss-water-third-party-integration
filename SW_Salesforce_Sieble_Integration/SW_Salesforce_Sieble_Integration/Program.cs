﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SHDocVw;
using SW_ProcessHandle;

namespace SW_Salesforce_Sieble_Integration
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
             
                if (args != null && args.Length > 0)
                {

                    ISiebelExecute executeSiebel;

                    string siebelPage = args[1];

                    if (siebelPage == "Schedule") // Siebel Schedule Page
                    {
                        string siebelUserName = args[2];
                        string siebelUserPassword = args[3];
                        string siebelRedirectPage = args[4];
                        string opportunityId = args[5];
                        string bundleProductId = args[6];
                        string productCode = args[7];
                        string serviceRegionNumber = args[8];
                        string opportunitNumber = args[9];
                        string isServiceRegionChanged = args[10];
                        executeSiebel = new SiebelScheduleUrl(siebelUserName, siebelUserPassword, siebelRedirectPage, opportunityId, bundleProductId, productCode, serviceRegionNumber,opportunitNumber,isServiceRegionChanged);

                    }
                    else // Siebel Company Page
                    {
                        string siebelCompanyName = args[2];
                        executeSiebel = new SiebelCompanyUrl(siebelCompanyName);
                    }

                    ProcessHandle processHandle = new ProcessHandle("iexplore", Config.SiebelExplorerTitleToClose) { };
                    processHandle.KillOldOpenedProcess();
                    executeSiebel.Execute();
                    
                }
                /*
                OpenExplorerInPost();
                if (args != null && args.Length > 0)
                {
                    //OpenExplorerInPost();
                    
                    String siebleurl = String.Format(@"http://{0}/callcenter_heb/start.swe?SWECmd=ExecuteLogin&SWEUserName={1}&SWEPassword={2}&SWEAC=SWECmd=InvokeMethod&SWEMethod=InboundOceanCall&SWEService=T4+Ocean+Integration+Service&Bambo=2&MyName=שרון&YourNamer=1212121ABC%$"
                    ,new string []{Config.SiebleEnvDomain,Config.SiebleUserName,Config.SiebleUserPassword });

                    Process.Start("IEXPLORE.EXE", siebleurl);
                    
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show("SW_Salesforce_Sieble_Integration Error : "+ ex.Message);
            }
            finally
            {
                Environment.Exit(1);
            }

        }

        private static void OpenExplorerInPost()
        {
            InternetExplorer IEControl = new InternetExplorer();
            IWebBrowserApp IE = (IWebBrowserApp)IEControl;
            IE.Visible = true;

            // Convert the string into a byte array
            ASCIIEncoding Encode = new ASCIIEncoding();
            //byte[] post = Encode.GetBytes(String.Format("SWECmd=ExecuteLogin&SWEUserName={0}&SWEPassword={1}&SWEAC=SWECmd=InvokeMethod&SWEMethod=InboundOceanCall&SWEService=T4+Ocean+Integration+Service", new string[] { Config.SiebleUserName, Config.SiebleUserPassword }));
            byte[] post = Encode.GetBytes(String.Format("SWECmd=ExecuteLogin&SWEUserName={0}&SWEPassword={1}&SWEAC=SWECmd=InvokeMethod&SWEMethod=InboundOceanCall&SWEService=T4+Ocean+Integration+Service", new string[] { Config.SiebelUserName, Config.SiebelUserPassword }));

            // The destination url
            string url = String.Format(@"http://{0}/callcenter_heb/start.swe", new string[] {Config.SiebelEnvDomain });

            // The same Header that its sent when you submit a form.
            string postHeaders = "Content-Type: application/x-www-form-urlencoded";
            //string postHeaders = "Content-Type: text/html";

            IE.Navigate(url, null, null, post, postHeaders);
        }
    }
}
