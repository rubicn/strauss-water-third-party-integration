﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SW_ProcessHandle
{
    public class ProcessHandle
    {
        private string ProcessName;
        private string TitleName;

        public ProcessHandle(string processName,string titleName)
        {
            this.ProcessName = processName;
            this.TitleName = titleName;
        }

        public void KillOldOpenedProcess()
        {
            Process[] currentRunningSibelProcess = GetExistProcessArray();
            int currentProcessId = Process.GetCurrentProcess().Id;
            if (currentRunningSibelProcess != null && currentRunningSibelProcess.Length > 0)
            {
                foreach (Process ctiIntegrationProcess in currentRunningSibelProcess)
                {
                    if (ctiIntegrationProcess.Id != currentProcessId)
                    {
                        ctiIntegrationProcess.Kill();
                    }
                }
            }
        }

        public Boolean IsSalesforceCompositeIntegrationRunning()
        {
            //this need to change to ProcessName == salesforceCTIIntegration
            //the composite bar must be inital before we start working
            Process[] sameAsthisSession = GetExistProcessArray();

            if (sameAsthisSession != null && sameAsthisSession.Length > 0)
            {
                return true;
            }

            return false;
        }

        private Process[] GetExistProcessArray()
        {
            Process[] runningProcesses = Process.GetProcesses();

            var currentSessionID = Process.GetCurrentProcess().SessionId;

            //this need to change to ProcessName == salesforceCTIIntegration
            //the composite bar must be inital before we start working
            Process[] sameAsthisSession = (from c in runningProcesses where c.SessionId == currentSessionID && c.ProcessName == this.ProcessName && c.MainWindowTitle.Contains(this.TitleName) select c).ToArray();

            return sameAsthisSession;
        }
    }
}
